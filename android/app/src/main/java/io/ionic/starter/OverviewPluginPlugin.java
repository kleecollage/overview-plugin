package io.ionic.starter;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;
import com.getcapacitor.JSObject;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;

@CapacitorPlugin(name = "OverviewPlugin")
public class OverviewPluginPlugin extends Plugin {

  private OverviewPlugin implementation = new OverviewPlugin();

  @PluginMethod()
  public void echo(PluginCall call) {
    String value = call.getString("value");

    JSObject ret = new JSObject();
    ret.put("value", value);
    call.resolve(ret);
  }

  @PluginMethod
  public void simulateWindowFocusChanged(PluginCall call) {
    boolean hasFocus = call.getBoolean("hasFocus");
    Log.d("my_app", "simulateWindowFocusChanged() called");

    if (!hasFocus) {
      Activity activity = getActivity();
      Log.d("my_app", "if(!hasFocus) called");

      try {
        if (activity instanceof AppCompatActivity) {
          AppCompatActivity appCompatActivity = (AppCompatActivity) activity;
          Log.d("Plugin_test", "simulateWindowFocusChanged() Inflating overlay");

          int layoutId = activity.getResources().getIdentifier("overlay_layout", "layout", activity.getPackageName());
          if (layoutId == 0) {
            Log.e("my_app", "Layout resource not found");
            call.reject("Failed to find overlay_layout");
            return; // Return early in case of error
          }
          View overlayView = LayoutInflater.from(activity).inflate(layoutId, null);
          // Use runOnUiThread to perform UI operations on the main thread
          appCompatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
              appCompatActivity.setContentView(overlayView);

              ImageView imageView = overlayView.findViewById(R.id.imageView);
              if (imageView != null) {
                Log.d("my_app", "ImageView found");
                Drawable drawable = activity.getResources().getDrawable(R.drawable.img);
                imageView.setImageDrawable(drawable);
                call.resolve();
              } else {
                Log.e("my_app", "Could not find ImageView");
                call.reject("Failed to find ImageView");
              }
            }
          });
        } else {
          Log.e("my_app", "Activity is not AppCompatActivity");
          call.reject("Activity is not AppCompatActivity");
        }
      } catch (Resources.NotFoundException e) {
        Log.e("my_app", "Resource not found: " + e.getMessage());
        call.reject("Resource not found: " + e.getMessage());
      } catch (Exception e) {
        Log.e("my_app", "An error occurred: " + e.getMessage());
        call.reject("An error occurred: " + e.getMessage());
      }
    }
    // Aqui se deberia resolver
    JSObject ret = new JSObject();
    ret.put("added", true);
    JSObject info = new JSObject();
    info.put("id", "unique-id-1234");
    ret.put("info", info);
    Log.d("my_app", "simulateWindowFocusChanged() finished");
    call.resolve(ret);
  }

}
