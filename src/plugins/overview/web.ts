import { WebPlugin } from '@capacitor/core';

import type { OverviewPlugin, WindowFocusChangeEvent } from './definitions';


export class OverviewWeb extends WebPlugin implements OverviewPlugin {
  
  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }

  async simulateWindowFocusChanged(callback: WindowFocusChangeEvent): Promise <void>  {} 
  
}

